import * as _ from 'lodash';
import { join } from './path';

export function get<T>(o: Object, path: string | (string | null | undefined)[]): T {
    const pathResult = typeof path === 'string' ? path : join(...path);
    return pathResult === '' ? o : _.get(o, pathResult);
}
