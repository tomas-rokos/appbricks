import * as _ from 'lodash';
import * as Path from './path';

describe('json/path', () => {
    it('lodash - set on path', () => {
        const o = {};
        expect(_.set(o, 'aa', 3)).toEqual({'aa': 3});
        expect(o).toEqual({'aa': 3});

        expect(_.set({}, 'a.b', 'x')).toEqual({'a': {'b': 'x'}});
        expect(_.set({}, 'a.b-e', 'x')).toEqual({'a': {'b-e': 'x'}});
        expect(_.set(o, 'aa.b-e', 'x')).toEqual({'aa': {'b-e': 'x'}});
        expect(_.set(o, 'a.b', {'x': 'y'})).toEqual({'aa': {'b-e': 'x'}, 'a': {'b': {'x': 'y'}}});
    });
    it('lodash - get on path', () => {
        expect(_.get('a', 'z', '-')).toEqual('-');
        expect(_.get('a', '', '-')).toEqual('-');

        expect(_.get({'a': 'b'}, '')).toEqual(undefined);

        const testingData = {
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'f'
                }
            }
        };

        expect(_.get(testingData, 'b.d.e')).toEqual('f');
        expect(_.get(testingData, 'b.d.f')).toEqual(undefined);
        expect(_.get(testingData, 'b.d..f')).toEqual(undefined);
    });
    it('.join - no value', () => {
        expect(Path.join(null)).toEqual('');
    });
    it('.join - single value', () => {
        expect(Path.join('a')).toEqual('a');
    });
    it('.join - more values', () => {
        expect(Path.join('a', 'b')).toEqual('a.b');
        expect(Path.join('a', 'b', 'c')).toEqual('a.b.c');
        expect(Path.join('a', 'b', 'c', 'd')).toEqual('a.b.c.d');
        expect(Path.join('a', 'b', 'c', 'd', 'e')).toEqual('a.b.c.d.e');
    });
    it('.join - skipped values', () => {
        expect(Path.join(null, 'b')).toEqual('b');
        expect(Path.join('a', null, 'c')).toEqual('a.c');
        expect(Path.join('a', null, null, 'd')).toEqual('a.d');
        expect(Path.join('a', null, 'c', null, 'e')).toEqual('a.c.e');
    });
    it('.join - empty values', () => {
        expect(Path.join('', 'b')).toEqual('b');
        expect(Path.join('a', '', 'c')).toEqual('a.c');
        expect(Path.join('a', '', '', 'd')).toEqual('a.d');
        expect(Path.join('a', '', 'c', '', 'e')).toEqual('a.c.e');
    });
    it('.keyAt - <26', () => {
        const keys = Path.alphaKeys(20);
        expect(keys(0)).toEqual('a');
        expect(keys(1)).toEqual('b');
        expect(keys(2)).toEqual('c');
        expect(keys(3)).toEqual('d');
        expect(keys(4)).toEqual('e');
        expect(keys(5)).toEqual('f');
        expect(keys(6)).toEqual('g');
        expect(keys(7)).toEqual('h');
        expect(keys(8)).toEqual('i');
        expect(keys(9)).toEqual('j');
        expect(keys(10)).toEqual('k');
        expect(keys(11)).toEqual('l');
        expect(keys(12)).toEqual('m');
        expect(keys(13)).toEqual('n');
        expect(keys(14)).toEqual('o');
        expect(keys(15)).toEqual('p');
        expect(keys(16)).toEqual('q');
        expect(keys(17)).toEqual('r');
        expect(keys(18)).toEqual('s');
        expect(keys(19)).toEqual('t');
        expect(keys(20)).toEqual('u');
        expect(keys(21)).toEqual('v');
        expect(keys(22)).toEqual('w');
        expect(keys(23)).toEqual('x');
        expect(keys(24)).toEqual('y');
        expect(keys(25)).toEqual('z');
    });
    it('.keyAt - ==26', () => {
        const keys = Path.alphaKeys(26);
        expect(keys(0)).toEqual('a');
    });
    it('.keyAt - ==27', () => {
        const keys = Path.alphaKeys(27);
        expect(keys(0)).toEqual('aa');
        expect(keys(1)).toEqual('ab');
        expect(keys(26)).toEqual('ba');
    });
    it('.keyAt - >26 && <=676', () => {
        const keys = Path.alphaKeys(600);
        expect(keys(0)).toEqual('aa');
        expect(keys(1)).toEqual('ab');
        expect(keys(2)).toEqual('ac');
        expect(keys(3)).toEqual('ad');
        expect(keys(4)).toEqual('ae');
        expect(keys(5)).toEqual('af');
        expect(keys(6)).toEqual('ag');
        expect(keys(7)).toEqual('ah');
        expect(keys(8)).toEqual('ai');
        expect(keys(9)).toEqual('aj');
        expect(keys(10)).toEqual('ak');
        expect(keys(11)).toEqual('al');
        expect(keys(12)).toEqual('am');
        expect(keys(13)).toEqual('an');
        expect(keys(14)).toEqual('ao');
        expect(keys(15)).toEqual('ap');
        expect(keys(16)).toEqual('aq');
        expect(keys(17)).toEqual('ar');
        expect(keys(18)).toEqual('as');
        expect(keys(19)).toEqual('at');
        expect(keys(20)).toEqual('au');
        expect(keys(21)).toEqual('av');
        expect(keys(22)).toEqual('aw');
        expect(keys(23)).toEqual('ax');
        expect(keys(24)).toEqual('ay');
        expect(keys(25)).toEqual('az');
        expect(keys(26)).toEqual('ba');
    });
    it('.keyAt - >676 && <=17576', () => {
        const keys = Path.alphaKeys(1000);
        expect(keys(0)).toEqual('aaa');
        expect(keys(1)).toEqual('aab');
        expect(keys(26)).toEqual('aba');
        expect(keys(676)).toEqual('baa');
    });
    it('.createRange', () => {
        expect(Path.createRange('a', 'b')).toEqual('a-b');
        expect(Path.createRange('aa', 'ab')).toEqual('aa-ab');
        expect(Path.createRange('aa-am', 'an')).toEqual('aa-an');
        expect(Path.createRange('aa-am', 'an-aq')).toEqual('aa-aq');
        expect(Path.createRange('am', 'an-aq')).toEqual('am-aq');
    });
    it('.createRange - nested', () => {
        expect(Path.createRange('c.a.a', 'c.a.b')).toEqual('c.a.a-b');
        expect(Path.createRange('c.a.aa', 'c.a.ab')).toEqual('c.a.aa-ab');
        expect(Path.createRange('c.a.aa-am', 'c.a.an')).toEqual('c.a.aa-an');
        expect(Path.createRange('c.a.aa-am', 'c.a.an-aq')).toEqual('c.a.aa-aq');
        expect(Path.createRange('c.a.am', 'c.a.an-aq')).toEqual('c.a.am-aq');
        expect(Path.createRange('c.a.am', 'c.b.an-aq')).toEqual(null);
    });
    it('.isInRange', () => {
        expect(Path.isInRange('ac-ae', 'aa')).toEqual(false);
        expect(Path.isInRange('ac-ae', 'ab')).toEqual(false);
        expect(Path.isInRange('ac-ae', 'ac')).toEqual(true);
        expect(Path.isInRange('ac-ae', 'ad')).toEqual(true);
        expect(Path.isInRange('ac-ae', 'ae')).toEqual(true);
        expect(Path.isInRange('ac-ae', 'af')).toEqual(false);
    });
    it('.parent', () => {
        expect(Path.parent(null)).toEqual('');
        expect(Path.parent('')).toEqual('');
        expect(Path.parent('aaa')).toEqual('');
        expect(Path.parent('a.b')).toEqual('a');
        expect(Path.parent('aaa.bbb')).toEqual('aaa');
        expect(Path.parent('a.b.c')).toEqual('a.b');
        expect(Path.parent('aaa.bbb.ccc')).toEqual('aaa.bbb');
    });
    it('.leaf', () => {
        expect(Path.leaf(null)).toEqual('');
        expect(Path.leaf('')).toEqual('');
        expect(Path.leaf('aaa')).toEqual('aaa');
        expect(Path.leaf('a.b')).toEqual('b');
        expect(Path.leaf('aaa.bbb')).toEqual('bbb');
        expect(Path.leaf('a.b.c')).toEqual('c');
        expect(Path.leaf('aaa.bbb.ccc')).toEqual('ccc');
    });
    it ('.next', () => {
        expect(Path.next(null)).toEqual('a');
        expect(Path.next('')).toEqual('a');
        expect(Path.next('a')).toEqual('b');
        expect(Path.next('m')).toEqual('n');
        expect(Path.next('ma')).toEqual('mb');
        expect(Path.next('mz')).toEqual('na');
        expect(Path.next('aaaaab')).toEqual('aaaaac');
        expect(Path.next('mnbqz')).toEqual('mnbra');
        expect(Path.next('aaa.bbb.a')).toEqual('aaa.bbb.b');
        expect(Path.next('aaa.bbb.aaab')).toEqual('aaa.bbb.aaac');
    });
});
