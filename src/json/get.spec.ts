import * as _ from 'lodash';
import { get } from './get';

describe('json/get', () => {
    const testingData = {
        'a': 'j',
        'b': {
            'c': 'c',
            'd': {
                'e': 'f'
            }
        }
    };
    it('root via string', () => {
        expect(get(testingData, '')).toEqual(testingData);
    });
    it('root via empty list', () => {
        expect(get(testingData, [])).toEqual(testingData);
    });
    it('root via list with empty string', () => {
        expect(get(testingData, [''])).toEqual(testingData);
    });
    it('root via list with null', () => {
        expect(get(testingData, [null])).toEqual(testingData);
    });
    it('single string', () => {
        expect(get(testingData, 'a')).toEqual('j');
        const x = get(testingData, 'a');
    });
});
