import * as _ from 'lodash';
import { set } from './set';

describe('json/set', () => {
    const testingDataBase = {
        'a': 'j',
        'b': {
            'c': 'c',
            'd': {
                'e': 'f'
            }
        },
        'c': [
            'q',
            'w',
            'e',
        ]
    };
    const testingData = {
        'a': 'j',
        'b': {
            'c': 'c',
            'd': {
                'e': 'f'
            }
        },
        'c': [
            'q',
            'w',
            'e',
        ]
    };
    it('top level via string', () => {
        const result = set(testingData, ['x'], 'v');
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'f'
                }
            },
            'c': [
                'q',
                'w',
                'e',
            ],
            'x': 'v'
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('top level via list', () => {
        const result = set(testingData, ['y'], 'q');
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'f'
                }
            },
            'c': [
                'q',
                'w',
                'e',
            ],
            'y': 'q'
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level via list', () => {
        const result = set(testingData, ['b', 'y'], 'q');
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'f'
                },
                'y': 'q'
            },
            'c': [
                'q',
                'w',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level via list set to null', () => {
        const result = set(testingData, ['b', 'd'], null);
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': null,
            },
            'c': [
                'q',
                'w',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level via list set to different subelem', () => {
        const result = set(testingData, ['b', 'd'], {'e': 'e'});
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'e'
                },
            },
            'c': [
                'q',
                'w',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level via list set to different subelem - performance', () => {
        const users = _.times(1000).map(i => ({ type: 'user', id: i, name: `user ${i}` }));
        const start = Date.now();
        users.reduce( (state, o) => {
            return set(state, [o.type, o.id.toString()], o);
        }, {});
        const end = Date.now();
        console.log(end - start);
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level via list with nested array ', () => {
        const result = set(testingData, ['c', '1'], 'a');
        expect(result).toEqual({
            'a': 'j',
            'b': {
                'c': 'c',
                'd': {
                    'e': 'f'
                },
            },
            'c': [
                'q',
                'a',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level object via list with number index ', () => {
        const result = set(testingData, ['b', '1'], 'a');
        expect(result).toEqual({
            'a': 'j',
            'b': {
                '1': 'a',
                'c': 'c',
                'd': {
                    'e': 'f'
                },
            },
            'c': [
                'q',
                'w',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
    it('nested level object via list with number index to do an array', () => {
        const result = set(testingData, ['b', '1'], 'a', true);
        expect(result).toEqual({
            'a': 'j',
            'b': [
                undefined,
                'a'
            ],
            'c': [
                'q',
                'w',
                'e',
            ],
        });
        expect(testingData).toEqual(testingDataBase);
    });
});
