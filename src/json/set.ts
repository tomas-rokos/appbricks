import * as _ from 'lodash';
import { join } from './path';

export function set(o: Object, path: string | (string | null | undefined)[], val: any, keepArray = false): Object {
    const pathResult = typeof path === 'string' ? path : join(...path);
    const delta = keepArray === true ? _.set({}, pathResult, val) : _.setWith({}, pathResult, val, Object);
    const obj = keepArray === true ?
            _.mergeWith({}, o, _.set({}, pathResult, null), delta)
            :
            _.mergeWith({}, o, _.setWith({}, pathResult, null, Object), delta);
    return obj;
}
