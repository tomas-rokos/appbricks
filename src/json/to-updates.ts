import * as _ from 'lodash';

import { join } from './path';

export default function(o: object): { [key: string]: any} {
    return toUpdatesWorker(o, '');
}

function toUpdatesWorker(o: object, path: string): {[key: string]: any} {
    const res = {};
    _.forIn(o, (val, key) => {
        if (typeof val === 'object') {
            _.merge(res, toUpdatesWorker(val, join(path, key)));
        } else {
            _.set(res, [join(path, key)], val);
        }
    });
    return res;
}
