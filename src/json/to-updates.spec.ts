import * as _ from 'lodash';
import toUpdates from './to-updates';

describe('json/toUpdates', () => {
    it('empty object', () => {
        expect(toUpdates({})).toEqual({});
    });
    it('first level', () => {
        expect(toUpdates({'a': 1, 'b': 'b', c: 'c', 'd': true, e: 3.14})).toEqual({'a': 1, 'b': 'b', 'c': 'c', 'd': true, 'e': 3.14});
    });
    it('nested level', () => {
        expect(toUpdates({'a': 1, 'b': { c: 'c', 'd': true}, e: 3.14})).toEqual({'a': 1, 'b.c': 'c', 'b.d': true, 'e': 3.14});
    });
});
