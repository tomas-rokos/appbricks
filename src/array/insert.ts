import * as _ from 'lodash';

export default function <T>(arr: T[], what: T): T[] {
    const idx = _.indexOf(arr, what);
    if (idx !== -1) {
        return arr;
    } else {
        return _.concat(arr, what);
    }
}
