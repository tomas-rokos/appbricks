import insert from './insert';

describe('array/insert', async () => {
    it('insert new', () => {
        expect(insert(['a', 'b'], 'c')).toEqual(['a', 'b', 'c']);
    });
    it('insert exising', () => {
        expect(insert(['a', 'b'], 'b')).toEqual(['a', 'b']);
    });
});
