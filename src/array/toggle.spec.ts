import * as _ from 'lodash';
import toggle from './toggle';

describe('array/toggle', async () => {
    it('remove', () => {
        expect(toggle(['a', 'b'], 'b')).toEqual(['a']);
    });
    it('add', () => {
        expect(toggle(['a', 'b'], 'c')).toEqual(['a', 'b', 'c']);
    });
});
