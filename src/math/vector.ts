export interface Vector {
    x: number;
    y: number;
}

export function add(base: Vector, what: Vector): Vector {
    return { x: base.x + what.x, y: base.y + what.y };
}

export function multiplyStatic(base: Vector, what: number): Vector {
    return { x: base.x * what, y: base.y * what };
}

export function substract(base: Vector, what: Vector): Vector {
    return { x: base.x - what.x, y: base.y - what.y };
}

export function divideStatic(base: Vector, what: number): Vector {
    return { x: base.x / what, y: base.y / what };
}

export function opposite(base: Vector): Vector {
    return { x: -base.x, y: -base.y };
}

export function length(base: Vector): number {
    return Math.sqrt(base.x * base.x + base.y * base.y);
}

export function normalize(base: Vector): Vector {
    return divideStatic(base, length(base));
}

export function angle(base: Vector): number {
    if (base.x === 0) {
        return base.y < 0 ? -Math.PI / 2 : Math.PI / 2;
    }
    if (base.y === 0) {
        return base.x < 0 ? Math.PI : 0;
    }
    const res = Math.acos(base.x / length(base));
    return base.y < 0 ? -res : res;
}
export function normal(base: Vector): Vector {
    return { x: -base.y, y: base.x };
}
