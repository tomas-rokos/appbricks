import * as _ from 'lodash';
import * as Vector from './vector';

describe('math/vector', async () => {
    it('add', () => {
        expect(Vector.add({ x: 10, y: 20 }, { x: 15, y: 25 })).toEqual({ x: 25, y: 45 });
    });
    it('multiplyStatic', () => {
        expect(Vector.multiplyStatic({x: 20, y: 30}, 2)).toEqual({x: 40, y: 60});
    });

    it('substract', () => {
        expect(Vector.substract({x: 15, y: 25}, {x: 20, y: 20})).toEqual({x: -5, y: 5});
    });
    it('multiplyStatic', () => {
        expect(Vector.multiplyStatic({x: 20, y: 30}, 2)).toEqual({x: 40, y: 60});
    });
    it('divedeStatic', () => {
        expect(Vector.divideStatic({x: 20, y: 10}, 2)).toEqual({x: 10, y: 5});
    });
    it('opposite', () => {
        expect(Vector.opposite({x: 20, y: 15})).toEqual({x: -20, y: -15});
    });
    it('length', () => {
        expect(Vector.length({x: 20, y: 15})).toEqual(25);
    });
    it('normalize', () => {
        expect(Vector.normalize({x: 20, y: 15})).toEqual({x: 0.8, y: 0.6});
    });
    it('angle', () => {
        expect(Vector.angle({x: 10, y: 0})).toEqual(0);
        expect(Vector.angle({x: 10, y: 5}) * 180 / Math.PI).toBeCloseTo(26.565);
        expect(Vector.angle({x: 10, y: 10}) * 180 / Math.PI).toBeCloseTo(45);
        expect(Vector.angle({x: 5, y: 10}) * 180 / Math.PI).toBeCloseTo(63.4349);
        expect(Vector.angle({x: 0, y: 10}) * 180 / Math.PI).toBeCloseTo(90);
        expect(Vector.angle({x: -5, y: 10}) * 180 / Math.PI).toBeCloseTo(116.565);
        expect(Vector.angle({x: -10, y: 10}) * 180 / Math.PI).toBeCloseTo(135);
        expect(Vector.angle({x: -10, y: 5}) * 180 / Math.PI).toBeCloseTo(153.4349);
        expect(Vector.angle({x: -10, y: 0}) * 180 / Math.PI).toBeCloseTo(180);
        expect(Vector.angle({x: -10, y: -5}) * 180 / Math.PI).toBeCloseTo(-153.4349);
        expect(Vector.angle({x: -10, y: -10}) * 180 / Math.PI).toBeCloseTo(-135);
        expect(Vector.angle({x: -5, y: -10}) * 180 / Math.PI).toBeCloseTo(-116.565);
        expect(Vector.angle({x: 0, y: -10}) * 180 / Math.PI).toBeCloseTo(-90);
        expect(Vector.angle({x: 5, y: -10}) * 180 / Math.PI).toBeCloseTo(-63.4349);
        expect(Vector.angle({x: 10, y: -10}) * 180 / Math.PI).toBeCloseTo(-45);
        expect(Vector.angle({x: 10, y: -5}) * 180 / Math.PI).toBeCloseTo(-26.565);
    });
    it('normal', () => {
        expect(Vector.normal({x: 20, y: 15})).toEqual({x: -15, y: 20});
    });
    it ('flow', () => {
        const r = _.flow(
            _.curryRight(Vector.add)({x: 0, y: 5}),
            Vector.angle
        );
        expect(r({x: 10, y: 5}) * 180 / Math.PI).toBeCloseTo(45);
    });
});
